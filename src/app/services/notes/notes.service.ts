import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from '../api';

@Injectable({
  providedIn: 'root'
})
export class NotesService {

  constructor(private http:HttpClient) { }

  get(id?) {
    return this.http.get(API+"seleccionar");
  }

  post(data?){
    console.log("Entramos al post "+data);
    return this.http.get(API+"insertar&&text="+data);
  }

  put(id,status){
    console.log("Entramos al post "+id+" status: "+status);
    
    return this.http.get(API+"actualizarStatus&nid="+id+"&status="+status);
  }

}