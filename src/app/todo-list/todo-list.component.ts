import { Component, OnInit } from '@angular/core';
import { NotesService } from '../services/api';
@Component({
  selector: 'todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
  _dobleB:any = "";
  todoItems:any = [];
  
  constructor(private notesService:NotesService) { 
    this.notesService.get().subscribe(
      (data)=>{
        for (let i = 0; i < data["length"]; i++) {
          var obj:Object = {};
          obj["id"] = data[i]["id"];
          obj["text"] = data[i]["text"];
          obj["isComplete"] = (data[i]["status"] == "0")? false: true;
          this.todoItems.push(obj); 
        }
      }
    )
  }

  set dobleB(value){
    this._dobleB = value.replace(/[a]/g, "_");;
  }

  get dobleB(){
    return this._dobleB;
  }

  ngOnInit() {
  }

  addTodoItem(e){
    if(e.keyCode != 13) return false;
    this.todoItems.push({
      text: e.target.value,/*this.dobleB,*/
      isComplete: false
    });
  
   // e.target.value = e.target.value;
    this.fnAdicionar(e.target.value);
  }

  markComplete(index,id){
    this.todoItems[index]["isComplete"] = !this.todoItems[index]["isComplete"];
    let estado =  (this.todoItems[index]["isComplete"] == true)?1:0;

    console.log(index+" id: "+id+" estadoA: "+estado);
    this.fnUpdate(id,estado);
  }

  fnAdicionar(data){
    console.log("entramos a la funcion "+data);
    this.notesService.post(data).subscribe(
      (data)=>{data}
    )
  }

  fnUpdate(id,status){
     console.log(id+"a"+status);
     this.notesService.put(id,status).subscribe();
  }

}
